<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Interview;
use App\Services\ResponseService;
use Illuminate\Http\Request;

class InterviewController extends Controller
{
    public function interviewPage(Request $request)
    {
        return view('pages/interview/interview', ['title' => '']);
    }

    public function createPage(Request $request)
    {
        return view('pages/interview/create-update', ['title' => 'Создание опроса']);
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'comment_for_manager' => 'string',
            'questions' => 'required|array|min:10',
        ]);

        $interview = new Interview;
        $interview->name = $request->name;
        $interview->comment_for_manager = $request->comment_for_manager ?? '';
        foreach ($request->questions as $questionNumber => $question) {
            $questionNumber++;
            if (count($question['questions_to_skip_numbers']) > 0) {
                foreach ($question['questions_to_skip_numbers'] as $questionNumber2) {
                    if ($questionNumber > $questionNumber2) return ResponseService::error("Пропускаемый вопрос $questionNumber2 должен идти после вопроса $questionNumber2");
                }
            }
        }
        $interview->questions = $request->questions;
        $interview->save();

        return '';
    }

    public function getQuestion(Request $request)
    {
        return '';
    }

    public function answer(Request $request)
    {
        return '';
    }
}
