<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InterviewWalkthrough extends Model
{
    //
    public $timestamps = false;

    protected $fillable = ['interview_id', 'answers', 'is_done', 'comment_from_manager'];

    protected $casts = [
        'answers' => 'array',
    ];
}
