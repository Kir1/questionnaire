<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    //
    protected $fillable = ['name', 'comment_for_manager', 'questions'];

    protected $casts = [
        'questions' => 'array',
    ];
}
