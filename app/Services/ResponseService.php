<?php

namespace App\Services;

class ResponseService
{
    public static function response($message = 'Запроc прошёл успешно', $code = 200, $fields = [])
    {
        return response()->json(array_merge([
            'code' => $code,
            'message' => $message
        ], $fields), $code);
    }

    public static function success()
    {
        return ResponseService::response('Создание прошло успешно', 201);
    }

    public static function error($message = 'Ошибка на сервере.', $code = 500, $fields = [])
    {
        return ResponseService::response($message, $code, $fields);
    }
}
