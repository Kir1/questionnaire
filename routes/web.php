<?php
use Illuminate\Support\Facades\Route;

Route::get('/', 'MainController@index');

Route::prefix('interview')->group(function () {
    Route::get('create', 'InterviewController@createPage');
    Route::post('create', 'InterviewController@create');
    Route::post('get-question', 'InterviewController@getQuestion');
    Route::post('answer', 'InterviewController@answer');
    Route::get('{id}', 'InterviewController@interviewPage');
});
