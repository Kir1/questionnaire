/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * vuetify
 */
import Vuetify from 'vuetify';
Vue.use(Vuetify);

/**
 */
import CreateUpdateForm from './components/interview/CreateUpdateForm';

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    components: {CreateUpdateForm}
});
