<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInterviewWalkthroughsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interview_walkthroughs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('interview_id');
            $table->json('answers');
            $table->boolean('is_done')->default(0);
            $table->string('comment_from_manager')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interview_walkthroughs');
    }
}
